- name: Net New Business Pipeline Created
  base_path: "/handbook/marketing/performance-indicators/"
  definition: The combined IACV of the net "New Business" opportunities. This data
    comes from a sheetload file.
  target:
  org: Marketing
  is_key: true
  public: false
  health:
    level: 0
    reasons:
    - tbd
  sisense_data:
    chart: 6592070
    dashboard: 431555
    embed: v2
- name: Pipe-to-spend per marketing activity
  base_path: "/handbook/marketing/performance-indicators/"
  definition: Pipeline (IACV on Opportunities) divided by the total marketing program
    spend, broken down by marketing activity.
  target:
  org: Marketing
  is_key: true
  public: true
  health:
    level: 0
    reasons:
    - tbd
- name: Marketing efficiency ratio
  base_path: "/handbook/marketing/performance-indicators/"
  definition: Marketing efficiency for a given month is calculated as the ratio of
    IACV from closed won opportunities for the current month and the 2 preceding months
    vs the Marketing Operating Expenses (IACV / Marketing Operating Expenses) for
    the same time period. GitLab's target is greater than 2.
  target:
  org: Marketing
  is_key: true
  public: true
  health:
    level: 0
    reasons:
    - tbd
- name: Opportunities per XDR per month created
  base_path: "/handbook/marketing/performance-indicators/"
  definition: The net number of unique opportunities that are created and have a "sales
    development representative" assigned/attached to the opportunity segmented by
    xDR and by month. This data comes from Salesforce.
  target:
  org: Marketing
  is_key: true
  public: true
  health:
    level: 0
    reasons:
    - tbd
  sisense_data:
    chart: 6222245
    dashboard: 431555
    embed: v2
- name: LTV / CAC ratio
  base_path: "/handbook/marketing/performance-indicators/"
  definition: The customer Life-Time Value to Customer Acquisition Cost ratio <a href="https://about.gitlab.com/handbook/sales/#ltv">LTV</a>:<a
    href="https://about.gitlab.com/handbook/sales/#customer-acquisition-cost-cac">CAC</a>
    measures the relationship between the lifetime value of a customer and the cost
    of acquiring that customer. <a href="https://www.klipfolio.com/resources/kpi-examples/saas-metrics/customer-lifetime-value-to-customer-acquisition-ratio">A
    good LTV to CAC ratio is considered to be > 3.0.</a>
  target:
  org: Marketing
  is_key: true
  public: true
  health:
    level: 0
    reasons:
    - tbd
- name: Social Media Followers
  base_path: "/handbook/marketing/performance-indicators/"
  definition: Follower growth across social channels is a good indicator or resonating
    with a wider audience or deeply connecting within a niche. Follower growth is
    defined as all net-new followers, across all GitLab-branded social media channels
    in a given period. This KPI is tracked by exporting reports and data from our
    social media management tool, Sprout Social. This data is then manually ingested
    into Sisense via a formatted Google Sheet. This KPI will evolve further as more
    data becomes available.
  target:
  org: Marketing
  is_key: true
  public: true
  health:
    level: 3
    reasons:
    - Currently, there is no target because we are in a benchmarking year and without
      previous strategic data.
  sisense_data:
    chart: 8146731
    dashboard: 621921
    embed: v2
- name: New Unique Web Visitors (about.gitlab.com)
  base_path: "/handbook/marketing/performance-indicators/"
  definition: New Users in Google Analytics, or the number of first-time users during
    the selected date range. <b>Note</b>, there is ongoing work in finalizing this
    KPI chart in Sisense (WIP); please reference the DataStudio chart, which is also
    accessible by the url below <iframe width="600" height="338" src="https://datastudio.google.com/embed/reporting/12FyakX5mE3BTqXO3QgniMNq0KP4LjyNG/page/JcPY"
    frameborder="0" style="border:0" allowfullscreen></iframe> <br />
  org: Marketing
  is_key: true
  public: true
  health:
    level: 0
    reasons:
    - We need to define if this includes docs, about, and forum or only uses about.gitlab.com
      subdomain. Then we need to set a target, preferable a year over year target
      to account for monthly seasonality.
  sisense_data:
    chart: 8490725
    dashboard: 431555
    embed: v2
  urls:
  - https://datastudio.google.com/embed/reporting/12FyakX5mE3BTqXO3QgniMNq0KP4LjyNG/page/JcPY
- name: Total Web Sessions (about.gitlab.com)
  base_path: "/handbook/marketing/performance-indicators/"
  definition: Total number of sessions in Google Analytics within the selected date
    range. These are visits to the web site that may include multiple pages. <b>Note</b>,there
    is ongoing work in finalizing this KPI chart in Sisense (WIP); please reference
    the DataStudio chart, which is also accessible by the url below <iframe width="600"
    height="338" src="https://datastudio.google.com/embed/reporting/1ycD6QqPYpm1bYAYoaeDTnIPZNrXI53zw/page/JcPY"
    frameborder="0" style="border:0" allowfullscreen></iframe> <br />
  org: Marketing
  is_key: true
  public: true
  health:
    level: 0
    reasons:
    - We need to define if this includes docs, about, and forum or only uses about.gitlab.com
      subdomain. Then we need to set a target, preferable a year over year target
      to account for monthly seasonality.
  sisense_data:
    chart: 8490690
    dashboard: 431555
    embed: v2
  urls:
  - https://datastudio.google.com/embed/reporting/1ycD6QqPYpm1bYAYoaeDTnIPZNrXI53zw/page/JcPY
- name: New hire location factor - Marketing
  base_path: "/handbook/marketing/performance-indicators/"
  parent: "/handbook/hiring/metrics/#new-hire-location-factor"
  definition: The average location factor of all newly hired team members within the
    last rolling 3 month as of the end of the period. (eg. If the current month is
    2019-09-01 then the calculation pulls months 2019-06-01 to 2019-08-31). Each division
    and department has their own new hire location factor target.
  target:
  org: Marketing
  is_key: true
  public: true
  health:
    level: 0
    reasons:
    - tbd
  sisense_data:
    chart: 6786698
    dashboard: 431555
    embed: v2
- name: Pipeline coverage
  base_path: "/handbook/marketing/performance-indicators/"
  definition: IACV of pipeline with close dates in a given period (quarter) divided by IACV
    target. 
  target:
  org: Marketing
  is_key: true
  public: true
  health:
    level: 0
    reasons:
    - tbd
- name: Lead follow-up response time
  base_path: "/handbook/marketing/performance-indicators/"
  definition: The amount of time (Days, hours, minutes) it takes for a Lead/Contact
    to move from MQL to another status - this indicates how long it took for a sales
    rep/xDR to respond to the record becoming a MQL.
  org: Marketing
  is_key: true
  public: true
  health:
    level: 0
    reasons:
    - tbd
- name: Qty. of Case Studies published per month
  base_path: "/handbook/marketing/performance-indicators/"
  org: Marketing
  is_key: true
  public: true
  health:
    level: 0
    reasons:
    - tbd
- name: 50% or more SOV compared to most published competitor
  base_path: "/handbook/marketing/performance-indicators/"
  definition: The comparison of the GitLab share of voice (SOV) percentage to most
    published competitors’ SOV percentage. SOV is the number of media mentions compared
    to the media mentions received by a competitor and then the percentage is calculated
    out of 100%. The SOV percentages are tracked via a media service called TrendKite
    by our PR agency.
  target:
  org: Marketing
  is_key: true
  public: true
  health:
    level: 2
    reasons:
    - We use TrendKite to track SOV and do not have ability to share public dashboard.
- name: Increase partner focused media coverage by 25% in FY21
  base_path: "/handbook/marketing/performance-indicators/"
  definition: The amount of media coverage/articles focused on partner and channel
    related topics in FY21 compared to FY20. Increasing the percentage of coverage
    increases GitLab’s brand awareness on the specific topic.
  target:
  org: Marketing
  is_key: true
  public: true
  health:
    level: 2
    reasons:
    - Do not have a public dashboard for coverage numbers.
- name: Increase amount of remote work focused media coverage by 50% in FY21 over
    FY20
  base_path: "/handbook/marketing/performance-indicators/"
  definition: The amount of media coverage/articles focused on remote work topics
    in FY21 compared to FY20. Increasing the percentage of coverage increases GitLab’s
    brand awareness on the specific topic.
  target:
  org: Marketing
  is_key: true
  public: true
  health:
    level: 2
    reasons:
    - Do not have a public dashboard for coverage numbers.
- name: Total number of MQLs by month
  base_path: "/handbook/marketing/performance-indicators/"
  definition: Total number of <a href="https://about.gitlab.com/handbook/sales/field-operations/gtm-resources/#sts=MQL%20Definition">Marketo
    Qualified</a> Leads per month. This data comes from a sheetload file.
  target:
  org: Marketing
  is_key: true
  public: false
  health:
    level: 0
    reasons:
    - tbd
  sisense_data:
    chart: 6592066
    dashboard: 431555
    embed: v2
- name: Product Downloads
  base_path: "/handbook/marketing/performance-indicators/"
  definition: Total downloads by installation method (Omnibus, Cloud native helm chart,
    Source, etc). This chart is populated with the versions.gitlab.com data.
  target:
  org: Marketing
  is_key: true
  public: true
  health:
    level: 0
    reasons:
    - tbd
  sisense_data:
    chart: 5542406
    dashboard: 428908
